#!/bin/bash

#  The "SHR" script aims to show valuable infomation
# about the server's current state and creates a
# predefined health check report.
#
#  THIS SCRIPT DOES NOT AND SHOULD NOT MODIFY
# OR CONFIGURE ANYTHING IN THE OS
#
#  This project is licensed under the GNU General
# Public License v2.0

# Server Information Variables
current_date=$(date +'%d/%B/%Y %H:%M')
current_uptime=$(uptime --pretty)
current_load=$(uptime | cut -d"e" -f4)
docker_status=$(systemctl is-active docker)
report_path="/tmp/healthcheck_report_$HOSTNAME_$(date +'%Y%m%d_%H-%M-%S').log"
cpu_load=$(awk '/cpu/{printf("%.2f%\n"), ($2+$4)*100/($2+$4+$5)}' /proc/stat |  awk '{print $0}' | head -1)
available_cpus=$(lscpu | grep "CPU(s):" | grep -v "NUMA")
mem_load=$(free | awk '/Mem/{printf("%.2f%"), $3/$2*100}')
swap_load=$(free | awk '/Swap/{printf("%.2f%"), $3/$2*100}' 2>/dev/null)
operating_system=$(cat /etc/redhat-release)
kernel_version=$(uname -r)
total_storage=$(df -x tmpfs -x devtmpfs | awk '(NR>1)' | awk 'BEGIN{TOTAL=0}{TOTAL+=$2}END{print TOTAL/1048576}')
consumed_storage=$(df -x tmpfs -x devtmpfs | awk '(NR>1)' | awk 'BEGIN{CONSUMED=0}{CONSUMED+=$3}END{print CONSUMED/1048576}')
consumed_percent=$(bc -l <<< "scale=3;($consumed_storage/$total_storage)*100")
last_patch=$(yum history | grep "I, U" | head -n 1 | cut -d"|" -f 3)
tabs="\t\t\t"

# Specific commands for RHEL 6, and Ubuntu
# Case RHEL 6 then chkconfig, case ubuntu no selinux, os version
running_services=$(systemctl --no-pager --state=running --type=service | grep "loaded units" | cut -d " " -f 1)

Help()
{
 # Display Help
 echo "This script shows the current health status of the server."
 echo
 echo "Syntax: shr -[s|h|p|f] <path> "
 echo "options:"
 echo "-f        Show full report (Does not save a file)"
 echo "-s        Show short report (Does not save a file)"
 echo "-h        Prints this help menu"
 echo "-p <path> Show the full report and save it to the provided path or /tmp"
 echo -e "\n"
 echo "Examples:"
 echo -e "Show the short report and save the long report to the provided path: \n"
 echo "shr -p /home/<user>/reports/ "
 echo -e "\nShow the full report and do not save it:\n"
 echo "shr -f"
}

print_general_report () {
  #  Show basic server's information and
  # report's absolute path
  echo "-------------------------------------------"
  echo "     Server Health Check Short Report"
  echo "-------------------------------------------"
  echo -e "HOST:$tabs$HOSTNAME"
  echo -e "OS:$tabs$operating_system"
  echo -e "Kernel:$tabs$kernel_version"
  echo -e "DATE:$tabs$current_date"
  echo -e "UPTIME:$tabs$current_uptime"
  echo -e "LOAD:$tabs$current_load"
  echo "$available_cpus"
  echo -e "RUNNING SERVICES:$tabs$running_services"
  echo -e "LAST PATCH DATE:\t$last_patch"
  echo -e "DOCKER:$tabs$docker_status"
  echo -e "CPU:$tabs$cpu_load"
  echo -e "RAM:$tabs$mem_load"
  echo -e "SWAP:$tabs$swap_load"
  echo -e "USED STORAGE:$tabs$consumed_percent%\n"
  df -H -x tmpfs -x devtmpfs
  echo -e "\n"
  echo "Most CPU consuming process:"
  ps -eo pid,comm,pcpu,rss,user,etime --sort=-pcpu | head -n 4
  echo "Most Memory consuming process:"
  ps -eo pid,comm,pcpu,rss,user,etime --sort=-rss | head -n 4
  echo "-------------------------------------------"
}

regular_user_report () {
  # Create a new report using non-root permissions
  echo "-------------------------------------------"
  echo "     Report requested by: $(whoami)"
  echo "-------------------------------------------"
  echo "Report time, uptime, and host information"
  date
  uptime
  hostnamectl
  echo "-------------------------------------------"
  echo "OS and Kernel version"
  uname -a
  cat /etc/os-release
  echo "-------------------------------------------"
  echo "RAM Memory"
  free -h
  echo "-------------------------------------------"
  echo "Most CPU consuming processes"
  ps -eo pid,comm,pcpu,rss,user,etime --sort=-pcpu | head -n 5
  echo "-------------------------------------------"
  echo "Most Memory consuming processes"
  ps -eo pid,comm,pcpu,rss,user,etime --sort=-rss | head -n 5
  echo "-------------------------------------------"
  echo "Disk utilisation"
  df -h
  echo "-------------------------------------------"
  echo "SELinux Status (Recommended: Enforcing)"
  getenforce
  echo "-------------------------------------------"
  echo "Provided Services"
  systemctl --no-pager list-units --type=service
  echo "-------------------------------------------"
  echo "Running Services"
  systemctl --no-pager list-units --type=service --state=running
  echo "-------------------------------------------"
  echo "NICs and IP addresses"
  ip a
  echo "-------------------------------------------"
  echo "Open ports"
  ss -tulpan
  echo "-------------------------------------------"
  echo "Package Management History"
  yum history
}

admin_user_report () {
  #  This section add information to the automated
  # report using root permissions
  echo "-------------------------------------------"
  echo "Available Volume Groups Space"
  sudo vgs
  echo "-------------------------------------------"
  echo "List Block Devices"
  sudo lsblk
  echo "-------------------------------------------"
  echo "FSTAB file"
  sudo cat /etc/fstab
  echo "-------------------------------------------"
  echo "Available Disks"
  sudo fdisk -l
  echo "-------------------------------------------"
  echo "Current Firewall Rules"
  sudo firewall-cmd --list-all
  echo "-------------------------------------------"
  echo "Number Containers Running"
  sudo docker ps 2>/dev/null | wc -l
  echo "-------------------------------------------"
  echo "Containers Running List"
  sudo docker ps 2>/dev/null
  echo "-------------------------------------------"
  echo "Current running processes"
  sudo ps aux
  echo "-------------------------------------------"
}

# Main
while getopts "hfps" option; do
  case $option in
    h)
      Help
      ;;
    s)
    print_general_report
    ;;
    f)
      regular_user_report
      if [[ $EUID == 0 ]]
      then
        admin_user_report
      fi
      print_general_report
      ;;
    p)
      requested_path="$2"
      if [[ -d "$requested_path" ]] && [ ! -f "$requested_path" ];
        then
          file_name="$requested_path/healthcheck_report_$(date +'%Y%m%d_%H-%M-%S').log"
          regular_user_report >> "$file_name"
          if [[ $EUID == 0 ]]
          then
            admin_user_report >> "$file_name"
          fi
          print_general_report >> "$file_name"
          echo -e "\nThe report is ready for you at: $requested_path\n"
          exit 0
      else
          regular_user_report >> "$report_path"
          if [[ $EUID == 0 ]]
          then
            admin_user_report >> "$report_path"
          fi
          print_general_report >> "$report_path"
          echo "The provided directory does not exist or it is a file"
          echo "Your report is now available at $report_path"
      fi
      ;;
    \?)
      echo "Error: Invalid option"
      ;;
  esac
done
if [ $OPTIND -eq 1 ];
then
  print_general_report
fi

## Author:
## Francisco Islas
## fislas@tuta.io
