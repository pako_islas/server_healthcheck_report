# Server Health Check Report

Bash script to report the current server status and get valuable information to recover it

## Getting started

This script shows the current health status of the server and creates a report of the server characteristics such as enabled services, running services, fstab, current running processes, available disks, etc. Currently, it is only compatible with RHEL > 7, RockyOS >= 8, Fedora > 33.

### Syntax

```bash
shr -[s|h|p|f] <path>

options:
-f        Show full report (Does not save a file)
-s        Show short report (Does not save a file)
-h        Prints this help menu
-p <path> Show the full report and save it to the provided path or /tmp
```

### Usage Examples:

Show the short report and save the long report to the provided path:

`shr -p /home/<user>/reports/`

Show the full report and do not save it:

`shr -f`

## Installation

Decompress the zip or tar file, copy the `shr.sh` to `/usr/bin`, rename it as `shr`, and give it execution permissions

### Example:

```bash
sudo cp /home/user/server_healthcheck_report/shr.sh /usr/bin
sudo mv /usr/bin/shr.sh /usr/bin/shr
sudo chmod a+x /usr/bin/shr
```

## Usage

+ Show the short report with `shr` or `shr -s`:

```bash
-------------------------------------------
     Server Health Check Short Report
-------------------------------------------
HOST:			test
OS:			 Red Hat Enterprise Linux 8.2 (Ootpa)
Kernel:			4.18.0-193.87.1.el8_2.x86_64
DATE:			26/July/2022 21:42
UPTIME:			up 15 minutes
LOAD:			 0.00, 0.00, 0.01
CPU(s):              1
RUNNING SERVICES:			27
DOCKER:			inactive
CPU:			4.95%
RAM:			8.16%
SWAP:
USED STORAGE:			15.000%

Filesystem                 Size  Used Avail Use% Mounted on
/dev/mapper/rootvg-rootlv  2.2G  1.3G  872M  60% /
/dev/mapper/rootvg-usrlv    11G  2.4G  8.4G  22% /usr
/dev/mapper/rootvg-tmplv   2.2G  500M  1.7G  24% /tmp
/dev/sda1                  520M  215M  305M  42% /boot
/dev/mapper/rootvg-varlv   8.6G  424M  8.2G   5% /var
/dev/mapper/rootvg-homelv  1.1G  181M  883M  18% /home
/dev/sda15                 519M  6.2M  513M   2% /boot/efi
/dev/sdb1                  7.4G   34M  7.0G   1% /mnt


Most CPU consuming process:
    PID COMMAND         %CPU   RSS USER         ELAPSED
   6188 systemd-hostnam  0.6  8496 root           00:29
    958 rngd             0.5  6628 rngd           14:50
      1 systemd          0.2 14056 root           15:00
Most Memory consuming process:
    PID COMMAND         %CPU   RSS USER         ELAPSED
    976 firewalld        0.0 41040 root           14:50
    977 sssd_nss         0.0 38048 root           14:50
   1100 tuned            0.0 31164 root           14:46
-------------------------------------------
```

+ Save the full report at /home/user/reports/ `shr -p /home/user/reports`

```bash
[fislas@rhel7 ~]$ shr -p /home/users/reports

The report is ready for you at: /tmp/reports/

```

+ Show the full report `sudo shr -f`

```bash
sudo shr -f | less
```

```
-------------------------------------------
HOST:                   rhel7
OS:                      Red Hat Enterprise Linux Server 7.9 (Maipo)
Kernel:                 3.10.0-1160.59.1.el7.x86_64
DATE:                   26/July/2022 22:49
UPTIME:                 up 8 minutes
LOAD:                    0.24, 0.18, 0.13
CPU(s):                1
RUNNING SERVICES:                       28
DOCKER:                 unknown
CPU:                    7.99%
RAM:                    9.31%
SWAP:
USED STORAGE:                   6.200%

Filesystem                 Size  Used Avail Use% Mounted on
/dev/mapper/rootvg-rootlv  2.2G   73M  2.1G   4% /
/dev/mapper/rootvg-usrlv    11G  1.6G  9.2G  15% /usr
/dev/sda2                  518M  126M  393M  25% /boot
/dev/mapper/rootvg-homelv  1.1G   34M  1.1G   4% /home
/dev/mapper/rootvg-tmplv   2.2G   34M  2.2G   2% /tmp
/dev/sda1                  525M   11M  514M   2% /boot/efi
/dev/mapper/rootvg-optlv   2.2G  147M  2.0G   7% /opt
/dev/mapper/rootvg-varlv   8.6G  184M  8.4G   3% /var
/dev/sdb1                  7.3G   34M  6.9G   1% /mnt


Most CPU consuming process:
  PID COMMAND         %CPU   RSS USER         ELAPSED
 2969 python           0.3 20540 root           07:52
    1 systemd          0.2  7108 root           08:09
  762 rngd             0.2  3224 root           08:04
Most Memory consuming process:
  PID COMMAND         %CPU   RSS USER         ELAPSED
  813 firewalld        0.1 33660 root           08:04
 2969 python           0.3 20540 root           07:52
 1232 tuned            0.0 20104 root           08:00
-------------------------------------------
-------------------------------------------
     Report requested by: root
-------------------------------------------
Report time, uptime, and host information
Tue Jul 26 22:49:42 UTC 2022
 22:49:42 up 8 min,  1 user,  load average: 0.24, 0.18, 0.13
   Static hostname: rhel7
         Icon name: computer-vm
           Chassis: vm
        Machine ID: a115dd5b0a7746ef998417f0fbea698e
           Boot ID: 557aaec4a1b6487496b604e89153e108
    Virtualization: microsoft
  Operating System: Red Hat Enterprise Linux Server 7.9 (Maipo)
       CPE OS Name: cpe:/o:redhat:enterprise_linux:7.9:GA:server
            Kernel: Linux 3.10.0-1160.59.1.el7.x86_64
      Architecture: x86-64
-------------------------------------------
OS and Kernel version
Linux rhel7 3.10.0-1160.59.1.el7.x86_64 #1 SMP Wed Feb 16 12:17:35 UTC 2022 x86_64 x86_64 x86_64 GNU/Linux
NAME="Red Hat Enterprise Linux Server"
VERSION="7.9 (Maipo)"
ID="rhel"
ID_LIKE="fedora"
VARIANT="Server"
VARIANT_ID="server"
VERSION_ID="7.9"
PRETTY_NAME="Red Hat Enterprise Linux Server 7.9 (Maipo)"
ANSI_COLOR="0;31"
CPE_NAME="cpe:/o:redhat:enterprise_linux:7.9:GA:server"
HOME_URL="https://www.redhat.com/"
BUG_REPORT_URL="https://bugzilla.redhat.com/"

REDHAT_BUGZILLA_PRODUCT="Red Hat Enterprise Linux 7"
REDHAT_BUGZILLA_PRODUCT_VERSION=7.9
REDHAT_SUPPORT_PRODUCT="Red Hat Enterprise Linux"
REDHAT_SUPPORT_PRODUCT_VERSION="7.9"
-------------------------------------------
:WARNING: fdisk GPT support is currently new, and therefore in an experimental phase. Use at your own discretion.

. . .

root      1234  0.0  0.0 115812   640 ?        Ss   22:41   0:00 /usr/bin/rhsmcertd
root      1481  0.0  0.0      0     0 ?        S    22:41   0:00 [jbd2/sdb1-8]
root      1482  0.0  0.0      0     0 ?        S<   22:41   0:00 [ext4-rsv-conver]
root      1535  0.0  0.4 238064 15956 ?        Ss   22:41   0:00 /usr/bin/python -u /usr/sbin/waagent -daemon
root      1536  0.0  0.1 222740  5864 ?        Ssl  22:41   0:00 /usr/sbin/rsyslogd -n
root      1547  0.0  0.0  25908   940 ?        Ss   22:41   0:00 /usr/sbin/atd -f
root      1549  0.0  0.0 126392  1676 ?        Ss   22:41   0:00 /usr/sbin/crond -n
root      1557  0.0  0.0 110208   860 tty1     Ss+  22:41   0:00 /sbin/agetty --noclear tty1 linux
root      1558  0.0  0.0 110208   872 ttyS0    Ss+  22:41   0:00 /sbin/agetty --keep-baud 115200,38400,9600 ttyS0 vt220
root      1573  0.0  0.0 108044   288 ?        Ss   22:41   0:00 rhnsd
root      1641  0.0  0.1 112984  4316 ?        Ss   22:41   0:00 /usr/sbin/sshd -D
root      2969  0.3  0.6 613048 20540 ?        Sl   22:41   0:01 python -u bin/WALinuxAgent-2.7.3.0-py2.7.egg -run-exthandlers
root      8738  0.0  0.0      0     0 ?        S    22:42   0:00 [kworker/0:4]
root      8739  0.0  0.0      0     0 ?        S    22:42   0:00 [kworker/0:5]
root      8833  0.0  0.1 158932  5860 ?        Ss   22:44   0:00 sshd: fislas [priv]
fislas    8838  0.0  0.0 158932  2620 ?        S    22:44   0:00 sshd: fislas@pts/0
fislas    8839  0.0  0.0 116440  3056 pts/0    Ss   22:44   0:00 -bash
root      8965  0.0  0.0      0     0 ?        S    22:47   0:00 [kworker/u2:2]
root      9155  0.0  0.0      0     0 ?        S    22:47   0:00 [kworker/0:1]
root      9206  0.2  0.1  24456  3908 ?        Ss   22:49   0:00 /usr/lib/systemd/systemd-hostnamed
root      9266  0.0  0.1 243496  4668 pts/0    S+   22:49   0:00 sudo shr -f
fislas    9267  0.0  0.0 110412  1020 pts/0    S+   22:49   0:00 less
root      9269  0.0  0.0 113288  1552 pts/0    S+   22:49   0:00 /bin/bash /bin/shr -f
root      9350  0.0  0.1 243496  4668 pts/0    S+   22:49   0:00 sudo ps aux
root      9352  0.0  0.0 155452  1860 pts/0    R+   22:49   0:00 ps aux
-------------------------------------------
```

## Compatibility

Currently the script only works for RHEL > 7, Centos > 7, and Fedora > 33

## Authors and acknowledgment

+ Francisco Islas
+ fislas@tuta.io

## License
GNU GPL v2
